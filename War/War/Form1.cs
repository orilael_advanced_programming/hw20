﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace War
{
    public partial class Form1 : Form
    {
        private System.Windows.Forms.PictureBox[] cards;
        private System.Windows.Forms.PictureBox blueCard;
        private FileInfo[] info;

        private TcpClient client;
        private NetworkStream clientStream;

        static private Thread listenThread;
        static private Thread read;

        private static AutoResetEvent waitHandle;
        private static EventWaitHandle ewh;

        private string name;
        private string input;
        private int counter;
        private int counter2;
        bool firstClick;

        public Form1()
        {
            input = "";
            counter = 0; //your score
            counter2 = 0; // enemie's score
            firstClick = true;
            cards = new PictureBox[10];  //ten cards
            waitHandle = new AutoResetEvent(false);
            ewh = new EventWaitHandle(false, EventResetMode.ManualReset);

            InitializeComponent();
            GenerateCards();
        }

        void GenerateCards()
        {
            DirectoryInfo dir = new DirectoryInfo("Assests/Resources/");
            info = dir.GetFiles("*.*"); //get all files in directory,the red_back and blue_back are in another directory

            blueCard = new PictureBox
            {
                Name = "picDynamicBlue",
                Image = War.Properties.Resources.card_back_blue,
                Location = new Point(370, 55),
                Size = new System.Drawing.Size(70, 102),
                SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
            };
            this.Controls.Add(blueCard);

           
            Point nextLocation = new Point(20, 229);
            for (int i = 0; i < 10; i++)
            {
                System.Windows.Forms.PictureBox currentPic = new PictureBox
                {
                    BorderStyle = BorderStyle.None,
                    Name = "picDynamic" + i,
                    Image = War.Properties.Resources.card_back_red,
                    Location = nextLocation,
                    Size = new System.Drawing.Size(70, 102),
                    SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom,
                    Enabled = false 
                };
                
                //when mouse is above card
                currentPic.MouseHover += delegate (object sender1, EventArgs e1)
                {
                    currentPic.BorderStyle = BorderStyle.FixedSingle;
                };
                currentPic.MouseLeave += delegate (object sender1, EventArgs e1)
                {
                    currentPic.BorderStyle = BorderStyle.None;
                };

                // assign an event to it
                currentPic.Click += delegate (object sender1, EventArgs e1)
                 {
                     Random rnd = new Random();
                     int num = rnd.Next(52);  //randomize num 0 - 51
                     if(firstClick)
                        blueCard.Image = War.Properties.Resources.card_back_blue;  //make blue card blue
                     for (int j = 0; j < 10; j++)
                     {
                         cards[j].Enabled = false;  //unclickable
                         cards[j].Image = War.Properties.Resources.card_back_red; //all red
                     }
                     currentPic.Image = Image.FromFile("Assests/Resources/" + info[num].Name); //set picture of pressed card
                     name = info[num].Name;
                     name = stringSend(name.Split('_')[0], name.Split('_')[2]);  //get code of card by name of file
                    
                     waitHandle.Set();  //notify thread that card was pressed
                 };

                // add the picture object to the form (otherwise it won't be seen)
                this.Controls.Add(currentPic);

                // calculate the location point for the next card to be drawn/added
                nextLocation.X += currentPic.Size.Width + 10;
                cards[i] = currentPic;
            }

        }


        private void connect()
        {
          
            client = new TcpClient();
            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
            try
            {
                client.Connect(serverEndPoint);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Socket Error",MessageBoxButtons.OK , MessageBoxIcon.Error);  //if cant connect
                this.Invoke((MethodInvoker)delegate { this.Close(); });
                return;
            }
            clientStream = client.GetStream();
            byte[] buffer;

            buffer = new byte[4];
            int bytesRead = clientStream.Read(buffer, 0, 4);
            input = new ASCIIEncoding().GetString(buffer);
            if (input.Equals("0000"))
            {
                this.Invoke((MethodInvoker)delegate
                {
                    for (int i = 0; i < 10; i++)
                    {
                        cards[i].Enabled = true;  //enable cards
                    }
                });
            }

            read = new Thread(new ThreadStart(reader)); //create thread
            read.IsBackground = true;
            read.Start();
            
            while (!input.Equals("2000"))
            {
                firstClick = true;  //change blue card to blue

                waitHandle.WaitOne();   //wait for user to press on card

                buffer = new ASCIIEncoding().GetBytes(name); //send your cards name
                clientStream.Write(buffer, 0, buffer.Length);
                clientStream.Flush();

                ewh.WaitOne();  //wait for reader to get a message
                ewh.Reset();    // not an automatic handler, need to reset

                this.Invoke((MethodInvoker)delegate
                {
                    blueCard.Image = Image.FromFile("Assests/Resources/" + stringGet(input)); //put picture of the enemy card
                });
                firstClick = false; //dont change card to blue

                if (("" + input[1] + input[2]).CompareTo(("" + name[1] + name[2])) < 0)
                {
                    counter++;
                    this.Invoke((MethodInvoker)delegate { Score.Text = "" + counter; });
                }
                else if (("" + input[1] + input[2]).CompareTo(("" + name[1] + name[2])) > 0)
                {
                    counter2++;
                    this.Invoke((MethodInvoker)delegate { OpScore.Text = "" + counter2; });
                }
                this.Invoke((MethodInvoker)delegate
                { 
                    for (int i = 0; i < 10; i++)
                    {
                        cards[i].Enabled = true;    //cards are pressable again
                    }
                });
            }
        }

        private void reader()  //Gets messages from server
        {
            while (true)
            {
                byte[] buffer = new byte[4];
                int bytesRead = clientStream.Read(buffer, 0, 4);
                input = new ASCIIEncoding().GetString(buffer);

                if (input.Equals("2000"))
                {
                    this.Invoke((MethodInvoker)delegate { listenThread.Abort(); this.Close(); });
                    return;
                }
                ewh.Set();  //notify the other function that a massege has recieved
            }
        }

        string stringSend(string number, string color)  //get code from cardname
        {
            char col = color[0];
            if (number.Equals("jack"))
            {
                number = "11";
            }
            else if (number.Equals("queen"))
            {
                number = "12";
            }
            else if (number.Equals("king"))
            {
                number = "13";
            }
            else if (number.Equals("ace"))
            {
                number = "01";
            }
            else if (number.Length == 1)
            {
                number = "0" + number;//2  --> 02
            }
            else
            {
                number = "10";
            }
            string data = "1" + number + col.ToString().ToUpper();  //1 + number of card + first letter of color
            return data;
        }

        string stringGet(string sendata)    //returns cardname from input
        {
            string data;
            string str = "" + sendata[1] + sendata[2];
            if (str.Equals("01"))
            {
                data = "ace";
            }
            else if (str.Equals("11"))
            {
                data = "jack";
            }
            else if (str.Equals("12"))
            {
                data = "queen";
            }
            else if (str.Equals("13"))
            {
                data = "king";
            }
            else if (str.Equals("10"))
            {
                data = "10";
            }
            else
            {
                data = sendata[2].ToString();
            }
            data += "_of_";

            if (sendata[3].Equals('H'))
            {
                data += "hearts";
            }
            else if (sendata[3].Equals('C'))
            {
                data += "clubs";
            }
            else if (sendata[3].Equals('D'))
            {
                data += "diamonds";
            }
            else
            {
                data += "spades";
            }

            for (int i = 0; i < info.Length; i++)
            {
                if (info[i].Name.Contains(data))
                {
                    return info[i].Name;
                }
            }
            return "0000";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            listenThread = new Thread(new ThreadStart(connect));
            listenThread.IsBackground = true;   //closes thread incase of quiting program
            listenThread.Start();

        }


        private void button1_Click(object sender, EventArgs e)
        {
            this.Close(); //calls Form1_FormClosing
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(read != null)
                read.Abort();
            if (client.Connected)
            {
                byte[] buffer = new ASCIIEncoding().GetBytes("2000");
                clientStream.Write(buffer, 0, buffer.Length);
                clientStream.Flush();   
                clientStream.Close();
                client.Close();
                exitMsg();
            }
        }

        private void exitMsg()
        {      
            Form2 form = new Form2(Score.Text, OpScore.Text);
            form.StartPosition = FormStartPosition.CenterParent;
            form.ShowDialog();
        }
    }
}
    

